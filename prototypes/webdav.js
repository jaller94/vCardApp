

const fs = require('fs');
const path = require('path');
const webdav = require('webdav');

const config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));

function countCards(password){
  const client = webdav(
    config.host + "addressbooks/",
    config.username, // username
    config.password // password
  );
  let dir_userbook = client.getDirectoryContents('/users/' + config.username + '/');
  dir_userbook.then((contents) => {
    console.log(JSON.stringify(contents, undefined, 4));
    for (const dir_book of contents) {
      readBook(client, dir_book.filename);
    }
  }).catch(() => {
    console.log('Failed to load user\'s book.');
  });
}

async function readBook(client, folder) {
  let dir_book = client.getDirectoryContents(folder);
  dir_book.then((contents) => {
    //console.log(JSON.stringify(contents, undefined, 4));
    for(const cardfile of contents) {
      saveCard(client, cardfile.filename);
    }
  }).catch((err) => {
    console.log('Failed to load book.');
  });
}

async function saveCard(client, filepath) {
  client.getFileContents(filepath).then((data) => {
    const filename = path.posix.basename(filepath);
    function handle(err) {
      if (err) {
        console.log(filename);
        console.log(err);
        return;
      }
      console.log('Saved ' + filename);
    }
    fs.writeFile('./vcards/' + filename, data, handle);
  }).catch((err) => {
    console.error('Failed to read file '+ filepath);
  });
}

countCards();
