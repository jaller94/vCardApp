docfolder = ./doc

doc:
	./node_modules/.bin/esdoc

clean:
	rm -fR $(docfolder)
