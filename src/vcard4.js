import {createOption} from './helper';

function vCard4PropertySex(value) {
	const select = document.createElement('select');
	select.name = 'gender';
	select.appendChild(createOption('', '(empty)'));
	select.appendChild(createOption('M', 'male'));
	select.appendChild(createOption('F', 'female'));
	select.appendChild(createOption('O', 'other'));
	select.appendChild(createOption('N', 'none or not applicable'));
	select.appendChild(createOption('U', 'unknown'));
	select.value = value;
	return select;
}

function vCard4PropertyUri(value) {
	const a = document.createElement('a');
	a.href = value;
	a.rel = 'external noopener noreferrer';
	a.textContent = value;
	return a;
}

function vCard4PropertyDateTime(value) {
	const time = document.createElement('time');
	const timeobj = vCard4TimetoDateTime(value);
	try {
		time.datetime = timeobj.toISOString();
		time.textContent = timeobj.toLocaleString();
	} catch (e) {
		time.textContent = value;
	}
	return time;
}

function vCard4TimetoDateTime(value) {
	const year   = Number.parseInt(value.substr(0,4), 10);
	// JavaScript month count starts at 0!!
	const month  = Number.parseInt(value.substr(4,2), 10)-1;
	const day    = Number.parseInt(value.substr(6,2), 10);
	const hour   = Number.parseInt(value.substr(9,2), 10);
	const minute = Number.parseInt(value.substr(11,2), 10);
	const second = Number.parseInt(value.substr(13,2), 10);
	return new Date(Date.UTC(year, month, day, hour, minute, second));
}

export function parseVCard4(text) {
	const regex = /(.+?)(;.+?)?:(.+)/i;
	const lines = text.split('\n');

	const properties = [];
	let line;
	let nextline = lines.shift();
	let going = true;

	while (going) {
		line = nextline;
		if (!line) {
			console.log(lines);
			break;
		}
		nextline = lines.shift();
		if (!nextline) {
			going = false;
		}

		while (/^[ \t]/.test(nextline)) {
			line += nextline.substring(1);
			nextline = lines.shift();
			if (!nextline) {
				going = false;
				break;
			}
		}

		// Remove all CRs. The vCard Line Break is defined to be CRLF but we splited
		// the line by only watching for LFs.
		line = line.replace(/\r/g,'');
		const match = line.match(regex);

		if (match === null) {
			console.log('ERROR: Line did not match a vCard line');
			break;
		}

		const property = {
			name: match[1],
			parameters: {},
			value: match[3]
		};

		const parameters_string = (match[2] || ';').substring(1);
		if (match[2]) {
			//TODO Unify the two cases of one and multiple parameters.
			if (parameters_string.indexOf(';')) {
				// case: multiple parameter
				for (const pair_string of parameters_string.split(';')) {
					const index = pair_string.indexOf('=');
					const key = pair_string.substring(0, index);
					const value = pair_string.substring(index+1);
					property.parameters[key] = value;
				}
			// case: only one parameter
			} else {
				const index = parameters_string.indexOf('=');
				const key = parameters_string.substring(0, index);
				const value = parameters_string.substring(index+1);
				property.parameters[key] = value;
			}
		}

		properties.push(property);
	}
	return properties;
}

export function vCard4ToInterface(card) {
	const infowrapper = document.getElementById('infowrapper');

	// clear div
	while (infowrapper.firstChild) {
		infowrapper.removeChild(infowrapper.firstChild);
	}

	document.getElementById('displayname').textContent = (card.displayName || 'Unnamed Card');

	if (card.photo) {
		const photo = document.createElement('img');
		photo.classList.add('photo');
		photo.src = card.photo.toHTML();
		infowrapper.appendChild(photo);
	}

	const ignoredproperties = 'BEGIN END PHOTO'.split(' ');
	for (const property of card.properties) {
		if (ignoredproperties.includes(property.name)) {
			continue;
		}
		console.log(property);

		const element = vCard4PropertyToInterface(property);

		if (element) {
			infowrapper.appendChild(element);
		}
	}
}

function vCard4PropertyToInterface(property) {
	const element = document.createElement('div');
	element.classList.add('property');

	const label = document.createElement('span');
	label.classList.add('label');
	label.textContent = property.name;
	element.appendChild(label);

	// properties
	if (property.parameters.length !== 0) {
		let parameters_string = '';
			for (const key in property.parameters) {
			if (property.parameters.hasOwnProperty(key)) {
				const value = property.parameters[key];
				parameters_string += ';' + key + '=' + value;
			}
		}
	}

	const value = document.createElement('span');
	value.classList.add('value');

	switch (property.name) {
		case 'GENDER':
			//FIXME The property value is GENDER-value = sex [";" text]
			value.appendChild(vCard4PropertySex(property.value));
			break;
		case 'REV':
			value.appendChild(vCard4PropertyDateTime(property.value));
			break;
		case 'URL':
			value.appendChild(vCard4PropertyUri(property.value));
			break;
		case 'BEGIN':
		case 'PHOTO':
		case 'END':
			break;
		default:
			{
				value.textContent = property.value;
			}
	}
	element.appendChild(value);
	return element;
}
