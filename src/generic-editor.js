import CardProperty from './vcard4/CardProperty';

/**
 * An abstract class describing an interface for displaying or editing a vCard.
 */
export class vCardInterface {
	constructor(parent, card) {
		this.parent = parent;
		this.card = card;
	}
}

/**
 * A simple editor interface for vCard.
 */
export default class GenericEditor extends vCardInterface {
	constructor(parent, card) {
		super(parent, card);

		this.lines = [];

		// Setup button to add a new line
		const btn_addLine = document.createElement('button');
		btn_addLine.textContent = 'Add Line';
		btn_addLine.accessKey = 'A';
		btn_addLine.addEventListener('click', (event) => {
			this.appendLine();
		});
		parent.appendChild(btn_addLine);

		// Setup button to add a new line
		const btn_save = document.createElement('button');
		btn_save.textContent = 'Update';
		btn_save.addEventListener('click', (event) => {
			this.updateCard();
		});
		parent.appendChild(btn_save);

		this.updateInterface();
	}

	updateInterface() {
		for (const key in this.card.properties) {
			this.lines[key] = this.appendLine(this.card.properties[key]);
		}
	}

	updateCard() {
		for (const key in this.lines) {
			this.card.properties[key] = line[key].getData();
		}
	}

	appendLine(property) {
		const line = new GenericEditorLine();
		line.setData(property);
		this.parent.appendChild(line.getElement());
	}
}

export class GenericEditorLine {
	constructor() {
		const template = document.getElementById('vcard-editfields-generic');
		this.element = document.importNode(template.content, true);
	}

	getData() {
		const property = {};
		property.name = getNameElement().value;
		property.parameters = CardProperty.parseParameters(getParametersElement().value);
		property.value = getValueElement().value;
		return property;
	}

	getElement() {
		return this.element;
	}

	getNameElement() {
		return this.element.querySelector('.name');
	}

	getParametersElement() {
		return this.element.querySelector('.parameters');
	}

	getValueElement() {
		return this.element.querySelector('.value');
	}

	setData(property) {
		if (property) {
			if (property.name) {
				this.getNameElement().value = property.name;
			}
			if (property.parameters) {
				let string = '';
				for(const key in property.parameters) {
					if (string !== '') {
						string += ';';
					}
					string += key + '=' + property.parameters[key];
					//debugger;
				}
				this.getParametersElement().value = string;
			}
			if (property.value) {
				this.getValueElement().value = property.value;
			}
		}
	}
}
