import {download} from './helper';
import GenericEditor from './generic-editor';
import {parseVCard4,vCard4ToInterface} from './vcard4';

import './global-interface-binding';

const APPLICATION_TITLE = 'vCardViewer';
const STRING_UNNAMEDCARD = 'Unnamed Card';

class vCardPropertyDescription {
	constructor(name, validator = null) {
		this.name = name;
		this.validator = validator;
	}

	validateValue(value) {
		if (this.validator) {
			return this.validator.test(value);
		}
		// no validation
		return true;
	}
}

class vCardImage {
	constructor(mime, encoding, data) {
		this.mime = mime;
		this.encoding = encoding;
		this.data = data;
	}

	toHTML() {
		if (this.encoding) {
			if (this.encoding === 'b' && this.mime) {
				return 'data:' + this.mime + ';base64,' + this.data;
			} else {
				console.log('WARN: unknown image encoding: ' + this.encoding);
			}
		}
		// Default case: Treat the data as a URL
		return this.data;
	}
}

function sortCards(a, b) {
	if (!a.displayName) {
		if (!b.displayName) {
			return 0;
		}
		return -1;
	}
	if (!b.displayName) {
		return 1;
	}

	const nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
	const nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
	if (nameA < nameB) {
		return -1;
	}
	if (nameA > nameB) {
		return 1;
	}

	// names must be equal
	return 0;
}

export class Card {
	constructor() {
		this.reset();
	}

	fromText(text) {
		this.reset();
		this.properties = parseVCard4(text);
		this.text = text;

		const fn = this.getProperty('FN');
		if (fn) {
			this.displayName = fn.value;
		}
		const photo = this.getProperty('PHOTO');
		// Process the photo if the tag is set
		if (photo) {
			if (photo.parameters.TYPE && photo.parameters.ENCODING) {
				// Determine mimetype because it is set
				let mimetype = null;
				if (photo.parameters.TYPE === 'JPEG') {
					mimetype = 'image/jpeg';
				} else if (photo.parameters.TYPE === 'PNG') {
					mimetype = 'image/png';
				}
				this.photo = new vCardImage(mimetype, photo.parameters.ENCODING, photo.value);
			} else {
				// Parameters or encoding are not set
				this.photo = new vCardImage(null, null, photo.value);
			}
		}
	}

	getProperty(propertyName) {
		for (const property of this.properties) {
			if (property.name === propertyName) {
				return property;
			}
		}
	}

	getPropertyAll(propertyName) {
		const array = [];
		for (const property of this.properties) {
			if (property.name === propertyName) {
				array.push(property);
			}
		}
		return array;
	}

	reset() {
		this.displayName = null;
		this.properties = [];
		this.text = "BEGIN:VCARD\nVERSION:4.0\nEND:VCARD";
		this.photo = null;
	}
}

export class CardIndex {
	constructor() {
		this.cards = [];
	}

	static unserialize(text) {
		const cardIndex = new CardIndex();
		const array = JSON.parse(text);
		for (const cardText of array) {
			const card = new Card();
			card.fromText(cardText);
			cardIndex.push(card);
		}
		return cardIndex;
	}

	clear() {
		this.cards = [];
	}

	push(card) {
		// Reject if the content is not a Card
		if (!(card instanceof Card)) {
			return false;
		}
		this.cards.push(card);

		this.sort();
		return true;
	}

	remove(card) {
		const index = this.cards.findIndex(function(current) {
			return (this === current);
		}, card);
		if (index === -1) {
			console.log('WARN: Couldn\'t find Card that was supposed to be removed ' +
				'from the CardIndex.');
			return;
		}
		this.cards.splice(index, 1);
	}

	serialize() {
		const array = [];
		for (const card of this.cards) {
			array.push(card.text);
		}
		return JSON.stringify(array);
	}

	sort() {
		this.cards.sort(sortCards);
	}
}

export function loadOnline(url) {
	const reqListener = function() {
		console.log(this.responseText);
		addCard(this.responseText);
	};

	const oReq = new XMLHttpRequest();
	oReq.addEventListener("load", reqListener);
	oReq.open("GET", url);
	oReq.send();
}

export function loadFromFiles() {
	const input_file = document.getElementById('input_file');
	for (const file of input_file.files) {
		loadFromFile(file)
	}
}

async function loadFromFile(file) {
	const fileReader = new window.FileReader();
	fileReader.readAsText(file);

	fileReader.addEventListener('loadend', (text) => {
		if (fileReader.error !== null) {
			alert(fileReader.error);
			return;
		}
		addCard(fileReader.result);
	})
}

export function newCard() {
	const contact = new Card();

	CARDINDEX.push(contact);

	// Store changes
	saveToLocalStorage();

	// Update UI
	updatePeoplePicker();
}

export async function addCard(text) {
	const card = new Card();

	if (text) {
		card.fromText(text);
	}

	CARDINDEX.push(card);

	// Store changes
	saveToLocalStorage();

	// Update UI
	updatePeoplePicker();
}

function deleteAll() {
	CARDINDEX.clear();

	// Store changes
	saveToLocalStorage();

	// Update UI
	updatePeoplePicker();
}

function deleteCard(card) {
	CARDINDEX.remove(card);

	// Store changes
	saveToLocalStorage();

	// Update UI
	updatePeoplePicker();
}

function updatePeoplePicker(cardIndex) {
	cardIndex = cardIndex || filteredGroup;

	const peoplepicker = document.getElementById('peoplepicker');
	if (!peoplepicker) {
		console.log('ERROR: Didn\'t find the peoplepicker!');
		return;
	}

	// clear div
	while (peoplepicker.firstChild) {
		peoplepicker.removeChild(peoplepicker.firstChild);
	}

	for (const card of cardIndex.cards) {
		const wrapper = document.createElement('div');
		const image = document.createElement('img');
		image.classList.add('image');
		image.classList.add('image_square');
		image.alt = '';
		if (card.photo) {
			image.src = card.photo.toHTML();
		}
		const labels = document.createElement('div');
		labels.classList.add('labels');
		const label = document.createElement('span');
		label.classList.add('label');
		label.textContent = (card.displayName || '(Unnamed Card)');
		const additional = document.createElement('span');
		additional.classList.add('additional');
		const email = card.getProperty('EMAIL');
		if (email) {
			additional.textContent = (email.value || '');
		}

		labels.appendChild(label);
		labels.appendChild(additional);

		const actions = document.createElement('div');
		actions.classList.add('actions');
		const action_delete = document.createElement('span');
		action_delete.classList.add('delete');
		actions.appendChild(action_delete);

		wrapper.addEventListener('click', (event) => {
			event.preventDefault();
			event.stopPropagation();
			displayCard(card);
		});

		action_delete.addEventListener('click', (event) => {
			event.preventDefault();
			event.stopPropagation();
			deleteCard(card);
		});

		wrapper.appendChild(image);
		wrapper.appendChild(labels);
		wrapper.appendChild(actions);
		peoplepicker.appendChild(wrapper);
	}
}

function displayCard(card) {
	window.currentCard = card;

	setView('view');

	//TODO Support more formats
	vCard4ToInterface(card);

	const title = (card.displayName || STRING_UNNAMEDCARD) + ' - ' + APPLICATION_TITLE;
	if (document.title !== title) {
		document.title = title;
	}
}

function setView(view) {
	const infowrapper = document.getElementById('infowrapper');
	const editview = document.getElementById('editview');
	if (view === 'view') {
		infowrapper.style.display = '';
		editview.style.display = 'none';
	} else if (view === 'edit') {
		infowrapper.style.display = '';
		editview.style.display = 'none';
		const parent = document.createElement('div');
		new GenericEditor(parent, window.currentCard);
		infowrapper.appendChild(parent);
	} else if (view === 'text') {
		restoreTextareaFromCard();
		infowrapper.style.display = 'none';
		editview.style.display = '';
	} else {
		console.log('WARN: Tried to set the view to an unknown value: ' + view);
	}
}

export function downloadCard(card) {
	card = card || window.currentCard;
	const filename = (card.displayName || STRING_UNNAMEDCARD) + '.vcf';
	download(filename, card.text);
}

export function updateCardFromTextarea() {
	const textarea = document.getElementById('editview-textarea');
	if (window.currentCard && textarea) {
		window.currentCard.fromText(textarea.value);
	}

	CARDINDEX.sort();

	// Store changes
	saveToLocalStorage();

	// Update UI
	updatePeoplePicker();
}

function restoreTextareaFromCard() {
	const textarea = document.getElementById('editview-textarea');
	if (window.currentCard && textarea) {
		textarea.value = window.currentCard.text;
	}
}

function loadFromLocalStorage() {
	if ('localStorage' in window) {
		if ('cardindex' in window.localStorage) {
			CARDINDEX = CardIndex.unserialize(window.localStorage.cardindex);
			resetPeoplePicker(CARDINDEX);
			updatePeoplePicker();
		}
	}
}

function saveToLocalStorage() {
	if ('localStorage' in window) {
		window.localStorage.cardindex = CARDINDEX.serialize();
	}
}

let sourceGroup = new CardIndex();
let currentFilter = null;
let filteredGroup = sourceGroup;
let filterFinished = true;

window.CARDINDEX = new CardIndex();
resetPeoplePicker(CARDINDEX);
window.currentCard = null;
loadFromLocalStorage();

function resetPeoplePicker(cardIndex) {
	sourceGroup = cardIndex;
	currentFilter = null;
	filteredGroup = sourceGroup;
	filterFinished = true;
}

export function globalCardSearch(text) {
	filteredGroup = [];
	filterFinished = false;
}

window.deleteAll = deleteAll;
window.downloadCard = downloadCard;
window.globalCardSearch = globalCardSearch;
window.loadFromFiles = loadFromFiles;
window.loadOnline = loadOnline;
window.newCard = newCard;
window.setView = setView;
window.updateCardFromTextarea = updateCardFromTextarea;
