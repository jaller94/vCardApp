'use strict';

let button_importFromFile = document.getElementById('button_importFromFile');
button_importFromFile.addEventListener('click', () => {
	loadFromFiles();
});
button_importFromFile.disabled = false;

let button_newCard = document.getElementById('button_newCard');
button_newCard.addEventListener('click', () => {
	newCard();
});
button_newCard.disabled = false;

let button_deleteAllCards = document.getElementById('button_deleteAllCards');
button_deleteAllCards.addEventListener('click', () => {
	deleteAll();
});
button_deleteAllCards.disabled = false;

let input_cardsearch = document.getElementById('input_cardsearch');
button_deleteAllCards.addEventListener('click', () => {
	const text = input_cardsearch.value;
	globalCardSearch(text);
});
