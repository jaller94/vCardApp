import CardProperty from './CardProperty';

describe('parameters', () => {
  test('decode parameters', () => {
    let teststring = 'TYPE=TEXT;TASK=HOME';

    let a = CardProperty.parseParameters(teststring);
    expect(a.TYPE).toBe('TEXT');
    expect(a.TASK).toBe('HOME');
  });

  test('encode parameters', () => {
    const params = {
      TYPE: 'TEXT',
      TASK: 'HOME',
    };
    let expected = 'TYPE=TEXT;TASK=HOME';
    expect(CardProperty.getParametersAsString(params)).toBe(expected);
  });
});

describe('values', () => {
  describe('decoding and encoding', () => {
    const decoded = 'Pittsburgh Pirates\r\n115 Federal St\r\nPittsburgh, PA 15212" ^';
    const encoded = 'Pittsburgh Pirates^n115 Federal St^nPittsburgh, PA 15212^" ^^';
    test('decoding', () => {
      expect(CardProperty.decodeParameterValue(encoded)).toBe(decoded);
    });
    test('encoding', () => {
      expect(CardProperty.encodeParameterValue(decoded)).toBe(encoded);
    });
  });
});
