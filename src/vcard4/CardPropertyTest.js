import Card from './Card';
import CardFile from './CardFile';
import CardProperty from './CardProperty';

let teststring = 'TYPE=TEXT;TASK=HOME';

let a = CardProperty.parseParameters(teststring);
if (a.TYPE !== 'TEXT') throw new Error('Value of TYPE does not match!');
if (a.TASK !== 'HOME') throw new Error('Value of TYPE does not match!');
if (a.getParametersAsString() !== teststring) throw new Error('getParametersAsString does not return the same!');

// Test en- and decoding a string
let decoded = 'Pittsburgh Pirates\r\n115 Federal St\r\nPittsburgh, PA 15212" ^';
let encoded = 'Pittsburgh Pirates^n115 Federal St^nPittsburgh, PA 15212^" ^^';
if (CardProperty.decodeParameterValue(encoded) !== decoded) throw new Error('Decoding failed!');
if (CardProperty.encodeParameterValue(decoded) !== encoded) throw new Error('Encoding failed!');

// Test decoding something that we encoded
if (CardProperty.decodeParameterValue(CardProperty.encodeParameterValue(decoded) === decoded)) throw new Error('En- and Decoding failed!');
if (CardProperty.decodeParameterValue(CardProperty.encodeParameterValue(encoded) === encoded)) throw new Error('En- and Decoding failed!');

// Test all escaping characters next to each other.
decoded = '"^\r\n^"\r\n""';
if (CardProperty.decodeParameterValue(CardProperty.encodeParameterValue(decoded) === decoded)) throw new Error('En- and Decoding failed!');

// Test new lines next to the character n (because a new line gets escaped to ^n).
decoded = 'n\r\nn"\r\n"n';
if (CardProperty.decodeParameterValue(CardProperty.encodeParameterValue(decoded) === decoded)) throw new Error('En- and Decoding failed!');

let card_string = `BEGIN:VCARD
VERSION:4.0
N:Forrest;Gump;;Mr.;
FN:Forrest Gump
ORG:Bubba Gump Shrimp Co.
TITLE:Shrimp Man
PHOTO;MEDIATYPE=image/gif:http://www.example.com/dir_photos/my_photo.gif
TEL;TYPE=work,voice;VALUE=uri:tel:+1-111-555-1212
TEL;TYPE=home,voice;VALUE=uri:tel:+1-404-555-1212
ADR;TYPE=WORK,PREF:;;100 Waters Edge;Baytown;LA;30314;United States of Amer
 ica
LABEL;TYPE=WORK,PREF:100 Waters Edge\nBaytown\, LA 30314\nUnited States of
 America
ADR;TYPE=HOME:;;42 Plantation St.;Baytown;LA;30314;United States of America
LABEL;TYPE=HOME:42 Plantation St.\nBaytown\, LA 30314\nUnited States of Ame
 rica
EMAIL:forrestgump@example.com
REV:20080424T195243Z
END:VCARD`;

card_string = card_string.replace('\n', '\r\n');

async function testFileAndCard() {
	const file = new CardFile();
	const cardFromString = new Card();

	await file.parseFile(card_string);
	const cardFromFile = file.getFileIndexOfCard(0);
	await cardFromString.parseCard(card_string);

	if (cardFromFile.getText() !== card_string) throw new Exception("File getText does not match source text!");
	if (cardFromFile.getText() !== cardText.getText()) throw new Exception("File and Card getText do not match!");
}
