import CardProperty from './CardProperty';

export default class Card {
	constructor() {
		this.reset();
	}

	reset() {
		this.file = null;
		this.fileindex = 0;
		this.displayName = null;
		this.properties = [];
		this.text = "BEGIN:VCARD\r\nVERSION:4.0\r\nEND:VCARD\r\n";
		this.photo = null;
	}

	getText() {
		return this.text;
	}

	getProperties() {
		return this.properties;
	}

	setText(text) {
		this.text = text;
	}

	setFile(file, fileindex = 0) {
		this.file = file;
		this.fileindex = fileindex;
	}

	pushProperty(property) {
		if (typeof property === 'object') {
			this.properties.push(property);
		} else if (typeof property === 'array') {
			// Push the items if it is an array.
			for (const item of propery) {
				pushProperty(item);
			}
		} else {
			console.log('Card.js; WARN; received an unknown type ' + typeof property +
				'for a property: '+ property);
		}
	}

	static async parseCard(text) {
		const regex = /(.+?)(;.+?)?:(.+)/i;
		const lines = text.split('\n');

		const properties = [];
		let line;
		let nextline = lines.shift();
		let going = true;

		while (going) {
			line = nextline;
			if (!line) {
				console.log(lines);
				break;
			}
			nextline = lines.shift();
			if (!nextline) {
				going = false;
			}

			while (/^[ \t]/.test(nextline)) {
				line += nextline.substring(1);
				nextline = lines.shift();
				if (!nextline) {
					going = false;
					break;
				}
			}

			// Remove all CRs. The vCard Line Break is defined to be CRLF but we splited
			// the line by only watching for LFs.
			line = line.replace(/\r/g,'');
			const match = line.match(regex);

			if (match === null) {
				console.log('ERROR: Line did not match a vCard line');
				break;
			}

			const property = {
				name: match[1],
				parameters: {},
				value: match[3]
			};

			const parameters_string = (match[2] || ';').substring(1);
			if (match[2]) {
				//TODO Unify the two cases of one and multiple parameters.
				if (parameters_string.indexOf(';')) {
					// case: multiple parameter
					for (const pair_string of parameters_string.split(';')) {
						const index = pair_string.indexOf('=');
						const key = pair_string.substring(0, index);
						const value = pair_string.substring(index+1);
						property.parameters[key] = value;
					}
				// case: only one parameter
				} else {
					const index = parameters_string.indexOf('=');
					const key = parameters_string.substring(0, index);
					const value = parameters_string.substring(index+1);
					property.parameters[key] = value;
				}
			}

			properties.push(property);
		}
		return properties;
	}
}
