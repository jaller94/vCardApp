//TODO incomplete list of property parameter defaults
const defaults = {
	'SOURCE': {'VALUE':'uri'},
	'KIND': {'VALUE':'text'},
	'XML': {'VALUE':'text'},
	'FN': {'VALUE':'text'},
	'N': {'VALUE':'text'},
	'NICKNAME': {'VALUE':'text'},
	'PHOTO': {'VALUE':'uri'},
	'BDAY': {'VALUE':'date-and-or-time'},
	'ANNIVERSARY': {'VALUE':'date-and-or-time'},
	'GENDER': {'VALUE':'text'},
	'ADR': {'VALUE':'text'},
	'TEL': {'VALUE':'text'},
	'EMAIL': {'VALUE':'text'},
	'IMPP': {'VALUE':'uri'},
	'LANG': {'VALUE':'language-tag'}
}

/**
 * A property object of a vCard4. It contains a name, optional parameters and
 * one or more values to be valid.
 */
export default class CardProperty {
	/**
	 * Constructor of a CardProperty
	 * @param {string} name A case-insensitive name of a property (eg. BDAY)
	 * @param {Object<string, string>} parameters A key-value store of parameters
	 * @param {string[]} values An array of values
	 * @property {string} name The name of the parameter
	 * @property {Object<string, string>} parameters A key-value store of parameters
	 * @property {string} values The values of the parameter
	 */
	constructor(name, parameters, values) {
		this.setName(name);
		this.setParameters(parameters);
		this.setValues(values);
	}

	/**
	 * Get a list of the default parameters for a given property.
	 * @param {string} name of the property
	 * @return {Object}
	 * @property {string} name
	 * @property {string} values
	 */
	static getDefaultParameters(name) {
		// Create an empty object that we're going to assign the defaults to.
		const result = {};

		// names are case-insensitive
		name = name.toUpperCase();
		if (typeof defaults[name] === 'object') {
			// Assign the defaults (creates a copy)
			Object.assign(result, defaults[name]);
		}
		return result;
	}

	/**
	 * Parse the parameters of a vCard property.
	 * @param {string} parameters_string - A string of vCard property parameters.
	 * @return {Object}
	 * @property {string} name
	 * @property {string} value
	 */
	static parseParameters(parameters_string) {
		const parameters = {};
		//TODO Unify the two cases of one and multiple parameters.
		// case: multiple parameter
		if (parameters_string.indexOf(';')) {
			for (const pair_string of parameters_string.split(';')) {
				const index = pair_string.indexOf('=');
				const key = pair_string.substring(0, index);
				const value = pair_string.substring(index+1);
				parameters[key] = value;
			}
		// case: only one parameter
		} else {
			const index = parameters_string.indexOf('=');
			const key = parameters_string.substring(0, index);
			const value = parameters_string.substring(index+1);
			parameters[key] = value;
		}
		return parameters;
	}

	/**
	 * Parameter value encoding according to RFC 6868 3.
	 * @param {string} value The parameter string
	 * @return {string} encoded representation of the string
	 */
	static encodeParameterValue(value) {
		value = value.replace(/\^/g, '^^'); // the escape character itself
		value = value.replace(/\r?\n/g, '^n'); // formatted text line breaks
		value = value.replace(/"/g, '^"'); // double-quote
		return value;
	}

	/**
	 * Parameter value decoding according to RFC 6868 3.
	 * @param {string} value Encoded parameter value string
	 * @return {string} the parameter value
	 */
	static decodeParameterValue(value) {
		value = value.replace(/\^\^/g, '^'); // the escape character itself
		value = value.replace(/\^n/g, '\r\n'); // formatted text line breaks
		value = value.replace(/\^"/g, '"'); // double-quote
		return value;
	}

  /**
	 * Returns a string with all parameters.
   * @param {object} the parameters
	 * @return {string} the string
	 */
	static getParametersAsString(parameters) {
		let string = '';
		for(const key in parameters) {
			if (string !== '') {
				string += ';';
			}
			string += key + '=' + parameters[key];
		}
		return string;
	}

	/**
	 * Returns an object with all parameters set including the default parameters
	 * of this property name.
	 * @return {string} - the name of the property (e.g. 'BDAY')
	 */
	getParametersWithDefaults() {
		const defaults = this.getDefaultParameters(this.name);
		// Return a copy of our object
		return Object.assign({}, defaults, this.parameters);
	}

	/**
	 * Get the property name.
	 * @return {string} - the name of the property (e.g. 'BDAY')
	 */
	getName() {
		return this.name;
	}

	/**
	 * Returns an object with all parameters.
	 * @return {Object} - a named list of parameters
	 */
	getParameters() {
		// Return a copy of our object
		return Object.assign({}, this.parameters);
	}

	/**
	 * Returns a string with all parameters.
	 * @return {string} the parameters of this object as a string
	 */
	getParametersAsString() {
		return CardProperty.getParametersAsString(this.parameters);
	}

	/**
	 * Returns a copy of the unescaped values.
	 * @return {string[]} - an array of all values
	 */
	getValues() {
		// Return a copy of our array
		return this.values.slice(0);
	}

	/**
	 * @param {string} name - the name of the property (e.g. BDAY).
	 */
	setName(name) {
		this.name = name;
	}

	/**
	 * @param {string,Object} parameters - named list of parameters.
	 */
	setParameters(parameters) {
		switch (typeof parameters) {
			case 'object':
				//TODO Check validity before import
				this.parameters = parameters;
				break;
			case 'string':
				this.parameters = CardProperty.parseParameters(parameters);
			default:
				console.log('WARN: unknown type as Property parameters: ' + typeof parameters);
		}
	}

	/**
	 * @param {string,string[]} values - list or values or escaped string.
	 */
	setValues(values) {
		switch (typeof parameters) {
			case 'array':
				//TODO Check validity before import
				this.parameters = parameters;
				break;
			case 'string':
				this.parameters = Property.parseValues(parameters);
			default:
				console.log('WARN: unknown type as Property parameters: ' + typeof parameters);
		}
	}
}
