import Card from './Card';

/**
 * A list of vCards which can be found in vCard file.
 * This class exists to update previously parsed vCards and to store whitespaces
 * between the vCards.
 */
export default class CardFile {
	constructor() {
		this.reset();
	}

	/**
	 * Clear all contents of this file.
	 */
	reset() {
		this.vcards = [];
	}

	/**
	 * Returns the index of the Card in this file. The index may change due to
	 * updates to the CardFile.
	 * @return {number} Zero-based integer indicating the position or -1 if the
	 * card is not part of this file.
	 */
	indexOf(card) {
		return this.vcards.indexOf(card);
	}

	/**
	 * Returns the text representation of this vCard file.
	 * @return {string} The raw vCard
	 */
	getText() {
		return this.text;
	}

	/**
	 * Updates the text representation of this vCard file. This causes a parser
	 * to interpret this file.
	 * @param {string} text - The raw vCard
	 */
	setText(text) {
		this.text = text;
	}

	/**
	 * Creates a new vCardFile object by inputting a raw text representation.
	 * @return {Promise<CardFile>}
	 */
	static async parseFile(text) {
		const file = new CardFile();
		const text_cards = text.split(/^BEGIN:VCARD$/);

		for (const text_card of text_cards) {
			let card = await Card.parseCard('BEGIN:VCARD\n' + text_card);
			this.cards.push(card);
		}

		return file;
	}
}
