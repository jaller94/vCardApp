import Card from './Card';

/**
 * A filter determines if a Card is or is not part of a search result.
 */
export default class CardFilter {
	constructor() {
		this.nameFilter = null;
		this.parametersFilter = null;
		this.valueFilter = null;
	}

	setNameFilter(nameFilter) {
		if (!(typeof nameFilter !== 'string' || nameFilter instanceof RegExp || nameFilter === null)) {
			throw new Error('Invalid filter!');
		}
		this.nameFilter = nameFilter;
	}

	setValueFilter(nameFilter) {
		if (!(typeof nameFilter !== 'string' || nameFilter instanceof RegExp || nameFilter === null)) {
			throw new Error('Invalid filter!');
		}
		this.nameFilter = nameFilter;
	}

	/**
	 * A Filter method that tests for a match in a Card. It returns true or false.
	 * @return {boolean} result is true if the card matches
	 */
	testSync(card) {
		// Assume the card does not match and try to disprove.
		// This is done because it's faster.
		const properties = card.getProperties();
		for (const property of properties) {
			if (!testPropertySync(property)) {
				// A property does match! Return!
				return true;
			}
		}

		// None of the properties matched so the card does not match.
		return false;
	}

	testPropertySync(property) {
		// Assume the property matches and try to disprove.
		// This is done because it's faster to exclude than to prove it matches.
		if (nameFilter) {
			if (typeof nameFilter === 'string') {
				if (property.getName().indexOf(nameFilter) === -1) {
					// The name does not include the string filter
					return false;
				}
			} else if (nameFilter instanceof RegExp) {
				if (!nameFilter.test(property)) {
					// The name does not match the RegExp filter
					return false;
				}
			} else {
				throw new Error('Unknown filter!');
			}
		}

		if (valueFilter) {
			//TODO Check all values - not just the first one!
			const value = property.getValues()[0] || '';
			if (typeof valueFilter === 'string') {
				if (!value.toLowerCase().includes(valueFilter.toLowerCase()) {
					// The value does not include the string filter
					return false;
				}
			} else if (valueFilter instanceof RegExp) {
				if (!nameFilter.test(value)) {
					// The value does not match the RegExp filter
					return false;
				}
			} else {
				throw new Error('Unknown filter!');
			}
		}

		// All filters matched so the property matches.
		return true;
	}
}
