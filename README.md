# vCardViewer
*!! For now this is just a prototype !!*

vCardViewer is a HTML5 vCard Reader / editor that does not tinker with your VCFs. It stores them in the browser's localStorage.

## Development
This project is still very young and there are a lot of uncertainties.

### Ongoing Goals
 * Always comply with the vCard4 standard (RFC 6350).
 * Don't randomly add or remove properties.

### Planned Features
 * Support sync via CardDAV (RFC 6352).
 * Work offline by setting up a Service Worker

## Attribution
 * Logo artwork is provided by [EmojiOne](https://www.emojione.com/) and is licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode).
